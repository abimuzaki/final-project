<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/new-article', 'IndexController@create');
Route::post('/new-article', 'IndexController@store');



Auth::routes();


Route::get('/{slash}', [
    'uses' => 'IndexController@singlepost',
    'as' => 'site.post'
]);