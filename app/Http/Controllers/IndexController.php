<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $data = Article::all();

        return view('index', ['data' => $data]);
    }

    public function create()
    {
        return view('createpost');
    }

    public function store(Request $request)
    {
        $articles = new Article();
        $articles->title = $request->title;
        $articles->body = $request->body;
        $articles->slash = Str::slug($request->title , '-');
        $articles->user_id = Auth::id();
        $articles->save();
        return 
        redirect('/');
        
    }

    public function singlepost($slash)
    {
        $article = Article::where('slash', '=', $slash)->first();
        return view ('singlepost', ['article' => $article]);
    }
}
