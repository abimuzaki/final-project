@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        @foreach ($data as $article)
        <div class="post-content">
        
            <a class="post-title" href="{{route('site.post', $article->slash)}}">
                {{$article->title}}
            </a>
            <div class="date-created">
                {{$article->created_at->format('d M Y')}}
            </div>
            <div class="post-body">
                {!!substr($article->body, -140)!!}
            </div>
            <div class="name-author">
                Author {{$article->user->name}}
            </div>
        </div>
    @endforeach
    </div>
    
</div>
</div>

@endsection