@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        <div class="post-title">
            {{$article->title}}
        </div>
        <div class="post-body mt-4">
            {!!$article->body!!}  
        </div>
    </div>
</div>
</div>

@endsection